'use strict'

const startBtn = document.getElementById('start')
const newGameBtn = document.getElementById('new-game')
const iters = document.getElementById('iterations')

// const info = document.getElementById('info')
const gridRows = 50
const gridColumns = 50

const world = document.getElementById('grid') // display
let present = Array(gridRows * gridColumns) // present state of the world
let future = Array(gridRows * gridColumns) // future state of the world

const ratio = 0.1 // alive to all cells ratio

let timerId = 0
let iterCounter = 0
let eventHandlersSet = false

const randomAlive = () => {
  // true -> the cell is alive
  return Math.random() < ratio ? 1 : 0
}

const sum = (...arr) => {
  let s = 0
  for (let d of arr) {
    s += d
  }
  return s
}

const getNeighbours = (i) => {
  // zwraca liczbę żywych komórek sąsiadujących z komórką o indeksie 'i' w tabeli 'present'
  const neighours = []

  if (i === 0) {
    neighours.push(
      present[i + 1],
      present[i + gridColumns],
      present[i + gridColumns + 1]
    )
  } else if (i === gridColumns - 1) {
    neighours.push(
      present[i - 1],
      present[i + gridColumns],
      present[i + gridColumns - 1]
    )
  } else if (i === gridRows * (gridColumns - 1)) {
    neighours.push(
      present[i + 1],
      present[i - gridColumns],
      present[i - gridColumns + 1]
    )
  } else if (i === gridRows * gridColumns - 1) {
    neighours.push(
      present[i - 1],
      present[i - gridColumns],
      present[i - gridColumns - 1]
    )
    // teraz lewa krawędź gridu
  } else if (i % gridColumns === 0) {
    neighours.push(
      present[i - gridColumns],
      present[i - gridColumns + 1],
      present[i + 1],
      present[i + gridColumns],
      present[i + gridColumns + 1]
    )
    // prawa krawędź gridu
  } else if ((i + 1) % gridColumns === 0) {
    neighours.push(
      present[i - gridColumns],
      present[i - gridColumns - 1],
      present[i - 1],
      present[i + gridColumns],
      present[i + gridColumns - 1]
    )
  } else if (i < gridColumns) {
    neighours.push(
      present[i - 1],
      present[i + 1],
      present[i + gridColumns - 1],
      present[i + gridColumns],
      present[i + gridColumns + 1]
    )
  } else if (i + gridColumns > gridRows * gridColumns) {
    neighours.push(
      present[i - 1],
      present[i + 1],
      present[i - gridColumns - 1],
      present[i - gridColumns],
      present[i - gridColumns + 1]
    )
  } else {
    neighours.push(
      present[i - gridColumns - 1],
      present[i - gridColumns],
      present[i - gridColumns + 1],
      present[i - 1],
      present[i + 1],
      present[i + gridColumns - 1],
      present[i + gridColumns],
      present[i + gridColumns + 1]
    )
  }
  return sum(...neighours)
}

function init() {
  let s = 0
  // clear world
  world.innerHTML = ''
  // generate present state & build the grid for displaying & and display
  for (let i = 0; i < gridRows * gridColumns; i++) {
    s = randomAlive()
    present[i] = s
    if (s) {
      world.insertAdjacentHTML('beforeend', '<div class="box alive"></div>')
    } else {
      world.insertAdjacentHTML('beforeend', '<div class="box"></div>')
    }
  }

  if (timerId) {
    clearInterval(timerId)
    timerId = 0
  }

  if (iterCounter) {
    iterCounter = 0
  }  

  if (!eventHandlersSet) {
    startBtn.addEventListener('click', startBtnClick)
    newGameBtn.addEventListener('click', newGameBtnClick)
    eventHandlersSet = true
  }
}

function displayWorld() {
  // displays present state of world
  for (let i = 0; i < gridRows * gridColumns; i++) {
    if (present[i]) {
      world.children[i].classList.add('alive')
    } else {
      world.children[i].classList.remove('alive')
    }
  }
}

function iterate() {

  let s = 0
  for (let i = 0; i < gridRows * gridColumns; i++) {
    s = getNeighbours(i)

    if (present[i]) {
      if (s < 2 || s > 3) {
        future[i] = 0
      } else {
        future[i] = 1
      }
    } else {
      if (s === 3) {
        future[i] = 1
      } else {
        future[i] = 0
      }
    }
  }
  
  [present, future] = [future, present]
  displayWorld()
  iterCounter++
  iters.textContent = iterCounter
}

function newGameBtnClick() {
  init()
  startBtn.textContent = 'Start'
}

function startBtnClick() {
  if (startBtn.textContent === 'Start') {
    timerId = setInterval(iterate, 300)
    startBtn.textContent = 'Stop'
  } else {
    clearInterval(timerId)
    timerId = 0
    startBtn.textContent = 'Start'
  }

}

init()


